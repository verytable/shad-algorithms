#include <iostream>
#include <vector>
#include <algorithm>

const int MAX_PLAYER_NUMBER = 100 * 1000;

/*
* Used instead std::pair<long long, int> for better interpretability
* */
struct FootballPlayer {
  long long efficiency;
  int number;

  FootballPlayer();

  FootballPlayer(long long efficiency_, int number_);

  const bool operator <(const FootballPlayer &other) const;
};

/*
* Sorts the elements in the range [begin, end) into ascending order
* The elements are compared using operator<
* */
template <typename TIter>
void MergeSort(const TIter begin, const TIter end);

/*
* Finds numbers of those players forming best team (returned as function argument)
* Returns maximal total efficiency
* */
long long FindMaxTotalEfficiency(
    const std::vector<FootballPlayer> &players,
    std::vector<int> *team
  );

/*
* Reads number of players and their efficiencies
* Constructs vector of FootballPlayers with those efficiencies and their numbers
* */
std::vector<FootballPlayer> ReadInput(std::istream &input_stream);

/*
* Prints maximal total efficiency
* and numbers of team players separated by space
* */
void PrintOutput(
    const long long max_total_efficiency,
    const std::vector<int> &team,
    std::ostream &output_stream
  );

int main() {
  std::ios_base::sync_with_stdio(false);
  std::vector<FootballPlayer> players = ReadInput(std::cin);
  MergeSort(players.begin(), players.end());
  std::vector<int> team;
  long long max_total_efficiency = FindMaxTotalEfficiency(players, &team);
  MergeSort(team.begin(), team.end());
  PrintOutput(max_total_efficiency, team, std::cout);
  return 0;
}
