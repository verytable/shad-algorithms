#include <iostream>
#include <vector>
#include <algorithm>

void printVector(const std::vector<int> &vector) {
  for (int i = 0; i < static_cast<int>(vector.size()); ++i) {
    std::cout << vector[i] << " ";
  }
  std::cout << "\n";
}

void filterByIndices(const std::vector<int> &sequence,
                     const std::vector<int> &indices,
                     std::vector<int> &result) {
  for (int i = 0; i < indices.size(); ++i) {
    result.push_back(sequence[indices[i]]);
  }
}

void solve(const std::vector<int> &sequence) {
  int sequence_length = static_cast<int>(sequence.size());
  std::vector< std::vector<int> > dp = {
    std::vector<int>(sequence_length, 1),
    std::vector<int>(sequence_length, 1)
  };
  std::vector< std::vector<int> > upper_paths(sequence_length);
  std::vector< std::vector<int> > lower_paths(sequence_length);
  for (int current_position = 0; current_position < sequence_length; ++current_position) {
    for (int previous_position = 0; previous_position < current_position; ++previous_position) {
      if (sequence[current_position] > sequence[previous_position]) {
        if (dp[0][current_position] < 1 + dp[1][previous_position]) {
          dp[0][current_position] = 1 + dp[1][previous_position];
          upper_paths[current_position] = lower_paths[previous_position];
        } else if (dp[0][current_position] == 1 + dp[1][previous_position]) {
          if (std::lexicographical_compare(lower_paths[previous_position].begin(),
                                           lower_paths[previous_position].end(),
                                           upper_paths[current_position].begin(),
                                           -1 + upper_paths[current_position].end())) {
            upper_paths[current_position] = lower_paths[previous_position];
          }
        }
      } else if (sequence[current_position] < sequence[previous_position]) {
        if (dp[1][current_position] < 1 + dp[0][previous_position]) {
          dp[1][current_position] = 1 + dp[0][previous_position];
          lower_paths[current_position] = upper_paths[previous_position];
        } else if (dp[1][current_position] == 1 + dp[0][previous_position]) {
          if (std::lexicographical_compare(upper_paths[previous_position].begin(),
                                           upper_paths[previous_position].end(),
                                           lower_paths[current_position].begin(),
                                           -1 + upper_paths[current_position].end())) {
            lower_paths[current_position] = upper_paths[previous_position];
          }
        }
      }
    }
    upper_paths[current_position].push_back(current_position);
    lower_paths[current_position].push_back(current_position);
  }

  int upper_end_max_length = *std::max_element(dp[0].begin(), dp[0].end());
  int lower_end_max_length = *std::max_element(dp[1].begin(), dp[1].end());

  std::vector< std::vector<int> > max_upper_paths;
  for (int i = 0; i < sequence_length; ++i) {
    if (dp[0][i] == upper_end_max_length) {
      max_upper_paths.push_back(upper_paths[i]);
    }
  }
  std::sort(max_upper_paths.begin(), max_upper_paths.end());

  std::vector< std::vector<int> > max_lower_paths;
  for (int i = 0; i < sequence_length; ++i) {
    if (dp[1][i] == lower_end_max_length) {
      max_lower_paths.push_back(lower_paths[i]);
    }
  }
  std::sort(max_lower_paths.begin(), max_lower_paths.end());

  std::vector<int> longest_alternating_sequence;
  if (upper_end_max_length > lower_end_max_length) {
    filterByIndices(sequence, max_upper_paths[0], longest_alternating_sequence);
  } else if (upper_end_max_length < lower_end_max_length) {
    filterByIndices(sequence, max_lower_paths[0], longest_alternating_sequence);
  } else {
    filterByIndices(sequence,
                    std::min(max_upper_paths[0], max_lower_paths[0]),
                    longest_alternating_sequence);
  }

  printVector(longest_alternating_sequence);
}

int main() {
  std::ios_base::sync_with_stdio(false);
  int sequence_length = 0;
  std::cin >> sequence_length;
  std::vector<int> sequence(sequence_length, -1);
  for (int i = 0; i < sequence_length; ++i) {
    std::cin >> sequence[i];
  }
  solve(sequence);
  return 0;
}
