#include <iostream>
#include <vector>
#include <algorithm>

struct Cube {
  int length;
  int width;
  int height;

  Cube(): length(0), width(0), height(0) {}

  const bool operator <(const Cube &other) const {
    return (length < other.length && width < other.width && height < other.height);
  }
};

void solve(const std::vector<Cube> &cubes) {
  int num_cubes = static_cast<int>(cubes.size());
  std::vector<int> dp(num_cubes, 1);
  for (int i = 0; i < num_cubes; ++i) {
    for (int j = 0; j < i; ++j) {
      if (cubes[j] < cubes[i]) {
        if (dp[i] < 1 + dp[j]) {
          dp[i] = 1 + dp[j];
        }
      }
    }
  }
  int max_nested_cubes_number = *std::max_element(dp.begin(), dp.end());
  std::cout << max_nested_cubes_number;
}

int main() {
  std::ios_base::sync_with_stdio(false);
  int num_cubes = 0;
  std::cin >> num_cubes;
  std::vector<Cube> cubes(num_cubes, Cube());
  for (int i = 0; i < num_cubes; ++i) {
    std::cin >> cubes[i].length >> cubes[i].width >> cubes[i].height;
  }
  solve(cubes);
  return 0;
}
