#include <iostream>
#include <vector>
#include <deque>
#include <utility>

void solve(const std::vector<int> &numbers, const std::vector<char> &moves) {
  int number_of_moves = moves.size();
  std::deque<int> window = {{numbers[0]}};
  std::deque<std::pair<int, int>> next_left_maximums;
  int right_pointer = 0;
  int left_pointer = 0;
  for (int move_number = 0; move_number < number_of_moves; ++move_number) {
    if (moves[move_number] == 'R') {
      ++right_pointer;
      int observed_number = numbers[right_pointer];
      while (!window.empty() && observed_number > window.back()) {
        window.pop_back();
      }
      int number_of_erased = 0;
      while (!next_left_maximums.empty() && observed_number >= next_left_maximums.back().first) {
        number_of_erased += next_left_maximums.back().second;
        next_left_maximums.pop_back();
      }
      next_left_maximums.push_back(std::make_pair(observed_number, number_of_erased + 1));
      window.push_back(observed_number);
      std::cout << window.front() << " ";
    } else {
      if (window.front() == numbers[left_pointer]) {
        window.pop_front();
      }
      ++left_pointer;
      std::cout << next_left_maximums.front().first << " ";
      next_left_maximums.front().second -= 1;
      if (next_left_maximums.front().second == 0) {
        next_left_maximums.pop_front();
      }
    }
  }
}

int main() {
  std::ios_base::sync_with_stdio(false);
  int array_length = 0;
  std::cin >> array_length;
  std::vector<int> numbers(array_length, 0);
  for (int i = 0; i < array_length; ++i) {
    std::cin >> numbers[i];
  }
  int number_of_moves = 0;
  std::cin >> number_of_moves;
  std::vector<char> moves(number_of_moves, 'R');
  for (int i = 0; i < number_of_moves; ++i) {
    std::cin >> moves[i];
  }
  solve(numbers, moves);
  return 0;
}
