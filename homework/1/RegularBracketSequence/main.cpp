#include <iostream>
#include <stack>
#include <string>

bool isOpening(const char bracket) {
  return bracket == '(' || bracket == '[' || bracket == '{';
}

char getOpeningBracket(const char bracket) {
  char opening_bracket = ' ';
  switch (bracket) {
    case ')':
      opening_bracket = '(';
      break;
    case ']':
      opening_bracket = '[';
      break;
    case '}':
      opening_bracket = '{';
      break;
    default:
      break;
  }
  return opening_bracket;
}

void solve(const std::string &str) {
  int str_length = static_cast<int>(str.length());
  std::stack<char> brackets;
  int position = 0;
  bool is_correct_brackets_sequence = true;
  while (position < str_length &&  is_correct_brackets_sequence) {
    char bracket = str[position];
    if (isOpening(bracket)) {
      brackets.push(bracket);
      ++position;
    } else if (!brackets.empty() && brackets.top() == getOpeningBracket(bracket)) {
      brackets.pop();
      ++position;
    } else {
      is_correct_brackets_sequence = false;
    }
  }
  if (position == str_length && brackets.empty()) {
    std::cout << "CORRECT";
  } else {
    std::cout << position;
  }
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::string str = "";
  std::cin >> str;
  solve(str);
  return 0;
}
