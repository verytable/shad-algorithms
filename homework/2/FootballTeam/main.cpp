#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

const int MAX_PLAYER_NUMBER = 100 * 1000;

long long FindMaxTotalEfficiency(
    const std::vector<std::pair<long long, int>> &players_with_index,
    std::vector<int> &team
  ) {
  int number_of_players = static_cast<int>(players_with_index.size());

  std::vector<long long> total_efficiency_of_prefixes(players_with_index.size() + 1, 0LL);
  for (int prefix_length = 1; prefix_length <= number_of_players; ++prefix_length) {
    total_efficiency_of_prefixes[prefix_length] =
      total_efficiency_of_prefixes[prefix_length - 1] +
      players_with_index[prefix_length - 1].first;
  }

  long long max_total_efficiency = players_with_index[0].first;
  int team_most_inefficient = 0;
  int team_most_efficient = 0;

  for (int most_inefficient = 0; most_inefficient < number_of_players - 1; ++most_inefficient) {
    long long max_efficiency =
      players_with_index[most_inefficient].first +
      players_with_index[most_inefficient + 1].first;

    int most_efficient = static_cast<int>(std::upper_bound(
      players_with_index.begin(),
      players_with_index.end(),
      std::make_pair(max_efficiency, MAX_PLAYER_NUMBER + 1)
    ) - players_with_index.begin()) - 1;

    long long total_efficiency =
      total_efficiency_of_prefixes[most_efficient + 1] -
      total_efficiency_of_prefixes[most_inefficient];

    if (total_efficiency > max_total_efficiency) {
      max_total_efficiency = total_efficiency;
      team_most_inefficient = most_inefficient;
      team_most_efficient = most_efficient;
    }
  }

  for (int i = team_most_inefficient; i <= team_most_efficient; ++i) {
    team.push_back(players_with_index[i].second);
  }

  return max_total_efficiency;
}

int main() {
  int number_of_players = 0;
  std::cin >> number_of_players;

  std::vector<std::pair<long long, int>> players_with_index(
    static_cast<size_t>(number_of_players),
    std::make_pair(0LL, 0)
  );

  long long efficiency = 0LL;
  for (int i = 0; i < number_of_players; ++i) {
    std::cin >> efficiency;
    players_with_index[i] = std::make_pair(efficiency, i + 1);
  }

  std::sort(players_with_index.begin(), players_with_index.end());

  std::vector<int> team;
  team.reserve(MAX_PLAYER_NUMBER);

  std::cout << FindMaxTotalEfficiency(players_with_index, team) << "\n";

  std::sort(team.begin(), team.end());

  for (int i = 0; i < static_cast<int>(team.size()); ++i) {
    std::cout << team[i] << " ";
  }

  return 0;
}
