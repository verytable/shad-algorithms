#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>
#include <iomanip>
#include <utility>

bool DoesExistCoveringCircle(
    const std::vector<std::pair<double, double>> &points,
    int number_of_points_to_cover,
    double radius
  ) {
  std::vector<std::pair<double, bool>> centers;
  centers.reserve(points.size() * 2);
  for (int i = 0; i < static_cast<int> (points.size()); ++i) {
    double x_ = points[i].first;
    double y_ = points[i].second;
    if (radius * radius > y_ * y_) {
      centers.push_back(std::make_pair(x_ - std::sqrt(radius * radius - y_ * y_), false));
      centers.push_back(std::make_pair(x_ + std::sqrt(radius * radius - y_ * y_), true));
    } else if (radius * radius == y_ * y_) {
      centers.push_back(std::make_pair(x_, false));
      centers.push_back(std::make_pair(x_, true));
    }
  }

  std::sort(centers.begin(), centers.end());

  int covered_points = 0;
  for (int i = 0; i < static_cast<int> (centers.size()); ++i) {
    if (centers[i].second) {
      --covered_points;
    } else {
      ++covered_points;
    }
    if (covered_points >= number_of_points_to_cover) {
      return true;
    }
  }
  return false;
}

double FindMinimalCoveringCircleRadius(
    const std::vector<std::pair<double, double>> &points,
    int number_of_points_to_cover,
    double min_radius,
    double max_radius,
    int steps
  ) {
  double mid = (min_radius + max_radius) / 2.0;
  if (steps == 0) {
    return mid;
  }
  if (DoesExistCoveringCircle(points, number_of_points_to_cover, mid)) {
    FindMinimalCoveringCircleRadius(points, number_of_points_to_cover, min_radius, mid, steps - 1);
  } else {
    FindMinimalCoveringCircleRadius(points, number_of_points_to_cover, mid, max_radius, steps - 1);
  }
}

int main() {
  int number_of_points = 0;
  std::cin >> number_of_points;

  int number_of_points_to_cover = 0;
  std::cin >> number_of_points_to_cover;

  std::vector<std::pair<double, double>> points(static_cast<size_t> (number_of_points));

  double x_ = 0.0;
  double y_ = 0.0;
  for (int i = 0; i < number_of_points; ++i) {
    std::cin >> x_ >> y_;
    points[i] = std::make_pair(x_, y_);
  }

  double minimal_covering_radius =
    FindMinimalCoveringCircleRadius(points, number_of_points_to_cover, 0.0, 2500.0, 50);

  std::cout << std::fixed << std::setprecision(6) << minimal_covering_radius;

  return 0;
}
