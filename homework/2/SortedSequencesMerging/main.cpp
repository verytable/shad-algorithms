#include <iostream>
#include <vector>
#include <algorithm>

const int INF = static_cast<int> (2e9);

void MergeTwoSortedSequences(
    int first_sequence_begin,
    int first_sequence_end,
    int second_sequence_begin,
    int second_sequence_end,
    std::vector<int> &sequence,
    std::vector<int> &buffer
  ) {
  int first_sequence_length = first_sequence_end - first_sequence_begin + 1;
  int second_sequence_length = second_sequence_end - second_sequence_begin + 1;

  int first_idx = first_sequence_begin;
  int second_idx = second_sequence_begin;

  int current_result_length = 0;

  while (current_result_length < first_sequence_length + second_sequence_length) {
    int first_elem = first_idx <= first_sequence_end ? sequence[first_idx] : INF;
    int second_elem = second_idx <= second_sequence_end ? sequence[second_idx] : INF;
    if (first_elem < second_elem) {
      ++first_idx;
    } else {
      ++second_idx;
    }
    buffer[current_result_length] = std::min(first_elem, second_elem);
    ++current_result_length;
  }

  for (int i = first_sequence_begin; i <= second_sequence_end; ++i) {
    sequence[i] = buffer[i - first_sequence_begin];
  }
}

void MergeSortedSequences(
    std::vector<int> &sequences,
    int sequence_length,
    int begin,
    int end,
    std::vector<int> &buffer
  ) {
  if (begin == end) {
    return;
  } else {
    int mid = (begin + end) / 2;
    MergeSortedSequences(sequences, sequence_length, begin, mid, buffer);
    MergeSortedSequences(sequences, sequence_length,  mid + 1, end, buffer);
    MergeTwoSortedSequences(
      begin * sequence_length,
      (mid + 1) * sequence_length - 1,
      (mid + 1) * sequence_length,
      (end + 1) * sequence_length - 1,
      sequences,
      buffer
    );
  }
}

int main() {
  std::ios_base::sync_with_stdio(false);

  int number_of_sequences = 0;
  std::cin >> number_of_sequences;

  int sequence_length = 0;
  std::cin >> sequence_length;

  std::vector<int> sequence;
  sequence.reserve(static_cast<size_t> (number_of_sequences * sequence_length));
  int elem = 0;
  for (int i = 0; i < number_of_sequences; ++i) {
    for (int j = 0; j < sequence_length; ++j) {
      std::cin >> elem;
      sequence.push_back(elem);
    }
  }

  std::vector<int> buffer(static_cast<size_t> (number_of_sequences * sequence_length), 0);
  MergeSortedSequences(sequence, sequence_length, 0, number_of_sequences - 1, buffer);

  for (int i = 0; i < static_cast<int> (sequence.size()); ++i) {
    std::cout << sequence[i] << " ";
  }

  return 0;
}
