#include <iostream>
#include <vector>
#include <algorithm>

int FindLongestCommonSubsequenceLength(
    const std::vector<int> &first_sequence,
    const std::vector<int> &second_sequence
  ) {
  int first_sequence_length = static_cast<int> (first_sequence.size());
  int second_sequence_length = static_cast<int> (second_sequence.size());
  std::vector<std::vector<int>> longest_common_subsequence_length_of_prefixes(
    first_sequence.size() + 1, std::vector<int>(second_sequence.size() + 1, 0)
  );
  for (int i = 1; i <= first_sequence_length; ++i) {
    for (int j = 1; j <= second_sequence_length; ++j) {
      if (first_sequence[i - 1] == second_sequence[j - 1]) {
        longest_common_subsequence_length_of_prefixes[i][j] =
          longest_common_subsequence_length_of_prefixes[i - 1][j - 1] + 1;
      } else {
        longest_common_subsequence_length_of_prefixes[i][j] =
          std::max(longest_common_subsequence_length_of_prefixes[i - 1][j],
                   longest_common_subsequence_length_of_prefixes[i][j - 1]);
      }
    }
  }
  return
    longest_common_subsequence_length_of_prefixes[first_sequence_length][second_sequence_length];
}

int main() {
  int first_sequence_length = 0;
  std::cin >> first_sequence_length;
  std::vector<int> first_sequence(first_sequence_length, 0);
  for (int i = 0; i < first_sequence_length; ++i) {
    std::cin >> first_sequence[i];
  }

  int second_sequence_length = 0;
  std::cin >> second_sequence_length;
  std::vector<int> second_sequence(second_sequence_length, 0);
  for (int i = 0; i < second_sequence_length; ++i) {
    std::cin >> second_sequence[i];
  }

  std::cout << FindLongestCommonSubsequenceLength(first_sequence, second_sequence);
  return 0;
}
