#include <iostream>

/*
* Виды тестирования:
*
*  1) unit
*  2) func
*  3) stress
*  4) b2b
* */

/*
* Unit  - модель белого ящика. Знаем код. Хотим уметь запуска для получения результата.
* А иногода хотим передавать дополнительные аргументы.
* это можно делать через argc, argv:
* int main(int argc, char *argv[])
*
* Напишем программу так, чтобы уметь запускать ./a.out --test:
* Для этого в main проверим аргументы:
*   if(argc == 2 && argv[i] == "--test") {
*     TestAll();
*     return 0;
*   }
*
* Как устроена TestAll?
* Рассмотрим на примере задачи сортировки множества чисел.
* На вход подается vector<int>. На выходе имеем vector<int>.
*
* Еще, понадобится функция сравнения.
* Напишем шаблонную функцию проверки на равенство:
*   template <typename T>
*   bool Compare(const T &first, const T &second, str::String name) {
*     if(first == second) {
*       std::cout << "OK " << mane << std::endl;
*       return true;
*     }
*     std::cout << "Fail" << std::endl
*     std::cout << first << std::endl;
*     std::cout <<< second << std::endl;
*   }
*
* Хотим уметь печатать вектор.
* Рассмотрим сперва пример с прошлого семинара:
* struct Point {
*   int x, y;
* };
*
* Хотим уметь писать std::cout << p; //где p - точка
*
*   friend std::ostream &operator << (std::ostream &os, const Point &p) {
*     os << "(" << p.x << ", " << p.y << ")";
*     return os; //поток изменяется, поэтому мы передавали его по ссылке
*   }
*
* Напишем аналогичную функцию для вектора:
*   template <typename T>
*   friend std::ostream &operator << (std::ostream &os, const std::vecotr<T> &data) {
*     for(auto it: data) { //
*       os << *it << ", ";
*     }
*     return os;
*   }
*
* std::vector<int> sort(std::vector<int> data) { //не const, ибо хотим менять элементы; не &, ибо не хотим менять вектор
*   for(int i = 0; i < dsata.size(); ++i) {
*     for(int j = 0; j < data.size() - 1; ++j) {
*       if(data[j] > data[j + 1] && data[j] != 1) {
*         swap(data[j], data[j + 1]);
*       }
*     }
*   }
*   return data;
* }
*
* void TestAll() {
*   Compare(sort({0, 1}), {0, 1}, "Some name");
*   Compare(sort{2, 4, 0, 1}), {0, 1, 2, 4}, "Other name"; //обнаружит ошибку
* }
*
* После запуска ./a.out --test
*   OK Some name
*   Fail Bar
*   2, 4, 0, 1
*   0, 1, 2, 4
* */

/*
* func - Функциональное тестирование
* Модель черного ящика. Не знаем, как устроена сама функция.
*
* Имеем текст main.cpp
* После компиляции имеем a.out
* В директории tests создадим несколько папок 01, 02 ...; А в них файлики с тестами и ответами
*   01/01.a, 02/02.a ...
*
* Для запуска напишем скрипт test.sh на Bash:
*   for t in tests/??
*   do
*     cp $t input.txt
*     ./a.out < input.txt > output.txt
*     diff output.txt $t.a
*   done
* */

/*
*  stress - тестирование
*  написали функцию, вычисляющую степень бинарно
*  int pow(int a, int n);
*
*  напишем функцию, делающей то же самое, но в которой трудно ошибиться
*  int pow_slow(int a, int n);
*
*  Будем вызывать функции на случайных элементах и сравнивать результаты.
*
*  int Random(int left, int right) { //[left, right)
*    return left + rand() % (right - left);
*  }
*
*  int main() {
*    while(...) {
*      int a = Random(-100, 100);
*      int n = Random(-100, 100);
*      if(pow(a) != pow_slow(a, n)) {
*        std::cout << a << " " << std::endl;
*        return 1;
*      }
*    }
*  }
*
* Другое применение:
*   часто можно достаточно просто понять по ответу, правильный ли он
*
* Еще один вид - нагрузочное тестирование
*   Оно вообще не смотрит на результат. Только на время, наличие ответа...
*   Для этого можно написать gen.cpp - генератор тестов.
*   Подавать результаты его вызова в main и использовать функции вроде time для отслеживания временя
* */

int main() {
  return 0;
}
